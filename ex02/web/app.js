const express = require('express');
const mongoose = require('mongoose');

const app = express();
const PORT = 3000;

const TestModel = mongoose.model("Test", { name: String });

mongoose
    .connect("mongodb://db:27017/my_database", {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    })
    .then(async () => {
        console.log("connected to mongodb");
        const testData = [
            { name: "test1" }, { name: "test2" }
        ];
        await TestModel.insertMany(testData); // Oczekiwanie na zakończenie wstawiania danych
        console.log("test data inserted successfully");
    })
    .catch((err) => {
        console.error("error connecting: ", err);
    });

app.get("/", async (req, res) => {
    try {
        const testDocuments = await TestModel.find({});
        res.json(testDocuments);
    } catch (err) {
        console.error('error fetching data: ', err);
        res.status(500).send("internal server error");
    }
});

app.listen(PORT, () => {
    console.log(`server is running on port ${PORT}`);
});
